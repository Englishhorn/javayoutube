package localhost;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CSVService {

  private final static Path CSV_PATH = Paths
      .get(System.getProperty("user.dir") + "/src/main/resources/songsTest.csv");

  public static List<Song> read() throws IOException {
    List<String> fileLines = Files.readAllLines(CSV_PATH);
    List<Song> songs = new ArrayList<>();

    for (String line : fileLines) {
      parseSong(line).ifPresent(songs::add);
    }

    System.out.println("\nParsed " + songs.size() + " songs!");

    return songs;
  }

  private static Optional<Song> parseSong(String line) {
    try {
      String[] fields = getFields(line);

      Song song = Song.builder()
          .artistName(fields[0])
          .trackName(fields[2])
          .danceability(fields[4])
          .duration(Long.parseLong(fields[5]))
          .energy(fields[6])
          .valence(fields[15])
          .build();

      return Optional.of(song);
    } catch (Exception e) {
      System.out.println("Couldn't parse line " + line);
      return Optional.empty();
    }
  }

  private static String[] getFields(String line) {
    String[] parts = line.split(",");
    String[] fields = new String[17];
    int x = 0;
    int i = 0;

    while (i < parts.length) {
      int ii = i + 1;
      while (parts[i].startsWith("\"") && !parts[i].endsWith("\"")) {
        parts[i] +=  "," + parts[ii++];
      }
      fields[x++] = parts[i].replaceAll("\"", "");
      i = ii;
    }

    return fields;
  }
}
