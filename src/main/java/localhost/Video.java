package localhost;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Video {

  private String id;

  private String title;

  private String channelName;

  private long duration;

  public boolean isValid(String performer, String songName, long desiredDuration) {

    performer = performer.toLowerCase();
    songName = songName.toLowerCase();

    System.out.print("\n" + channelName + ": " + title + "  -  ");

    if (!isStringValid(songName, title)) {
      System.out.print("Bad song name.");
      return false;
    }

    if (!isStringValid(performer, title) && !isStringValid(performer, channelName)) {
      System.out.print("Bad band name.");
      return false;
    }

    if (!isDurationValid(desiredDuration)) {
      System.out.print("Inappropriate duration. (" + duration + ")");
      return false;
    }

    System.out.print("OK!");
    return true;
  }

  private boolean isStringValid(String target, String resource) {

    String[] requiredParts = filterPunctuation(target).split(" ");

    for (String requiredPart : requiredParts) {
      if (requiredPart.length() < 2 || requiredPart.equals("ft") || requiredPart.equals("feat")) {
        continue;
      }
      if (!resource.contains(requiredPart)) {
        return false;
      }
    }

    if (!target.contains("live") && resource.contains("live")) {
      return false;
    }

    if (!target.contains("remix") && resource.contains("remix")) {
      return false;
    }

    if (!target.contains("cover") && resource.contains("cover")) {
      return false;
    }

    if (!target.contains("karaoke") && resource.contains("karaoke")) {
      return false;
    }

    return target.contains("parody") || !resource.contains("parody");
  }

  public boolean isDurationValid(long desiredDuration) {

    long allowedMinimum = Math.round(desiredDuration - 10000);
    long allowedMaximum = Math.round(desiredDuration + 10000);

    return duration >= allowedMinimum && duration <= allowedMaximum;
  }

  private String filterPunctuation(String original) {
    String result = original;

    for (String unwanted : Set.of("(", ")", "[", "]", "{", "}", "<", ">", "\"", ".", ",")) {
      result = result.replace(unwanted, "");
    }

    return result;
  }
}
