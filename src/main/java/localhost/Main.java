package localhost;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

public class Main {

  private String performer;
  private String songName;
  private long duration;

  private final SearchService searchService = new SearchService();
  private final DownloadService downloadService = new DownloadService();

  private static volatile List<Video> downloadedVideos = new ArrayList<>();

  public static void main(String[] args)
      throws IOException, ExecutionException, InterruptedException {

    ForkJoinPool customThreadPool = new ForkJoinPool(20);
    List<Song> songs = CSVService.read();

    customThreadPool.submit(() -> songs.parallelStream()
        //.skip(16)
        .limit(1)
        .filter(Song::validateDuration)
        .forEach(s -> {
          Main main = new Main();
          main.searchForSong(s);
        })).get();

    System.out.println("\n Successfully downloaded " + downloadedVideos.size() + " songs!");
  }

  public void searchForSong(Song song) {

    duration = song.getDuration();
    performer = song.getArtistName();
    songName = song.getTrackName();

    System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" +
        "\nSearching for " + performer + " - " + songName + ", which should be " + duration
        + "ms long.");

    List<Video> videos = new ArrayList<>();

    int limit = 12;

    while (videos.size() < limit--) {
      videos = searchService.findVideos(performer + " " + songName);
      System.out.println("Parsed " + videos.size() + "search results.");
    }
    while (videos.size() > 0) {
      Video video;

      try {

        video = findBestResult(videos);

      } catch (NoSuchElementException e) {
        System.out.println("\n" + e.getMessage());
        return;
      }
      System.out.println(
          "\n\nBest search result is " + video.getChannelName() + ": " + video.getTitle()
              + ", downloading...");

      try {
        downloadService.download(video, performer + " - " + songName);
        downloadedVideos.add(video);
        System.out.println("SUCCESS!");
        return;
      } catch (InterruptedException | IOException e) {
        videos.remove(video);
        System.out.println("\nCached error while downloading video! Let's try another!");
      }
    }

    System.out.println("\nThere's no appropriate videos left, download is failed for this song!");
  }

  private Video findBestResult(List<Video> videos) {
    System.out.println("\nVALIDATING...");

    List<Video> filteredVideos = videos.stream()
        .distinct()
        .filter(v -> v.isValid(performer, songName, duration))
        .collect(Collectors.toList());

    if (filteredVideos.size() == 0) {
      throw new NoSuchElementException("\nCould not find appropriate video!");
    }

    for (Video video : filteredVideos) {
      if (video.getTitle().contains("official")) {
        return video;
      }
    }

    for (Video video : filteredVideos) {
      if (video.getChannelName().contains("official")) {
        return video;
      }
    }

    return filteredVideos.get(0);
  }
}